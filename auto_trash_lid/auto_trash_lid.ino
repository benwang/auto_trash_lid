#include <Servo.h>

// Door definitions
const int DOOR_OPEN_DIST_THRESH = 15; // cm
const int DOOR_OPEN_DURATION = 2 * 1000; // ms

// Pin definitions
const int PIN_ULTSNC_TRIG = 8;
const int PIN_ULTSNC_ECHO = 7;
const int PIN_LED         = 6;
const int PIN_SERVO       = 9;

// Ultrasonic constants
const int DLY_ULTSNC_TRIG = 10; // us
const int ULTSNC_TRIG_TIMEOUT = 60000; // us

const int TO_INCHES = 148;
const int TO_CM = 58;

// Servo definitions
#define DEG_DELTA -3
const int DEG_SRV_OPEN = 85 + DEG_DELTA; // deg
const int DEG_SRV_CLSD1 = 12 + DEG_DELTA; // deg
const int DEG_SRV_CLSD2 = 17 + DEG_DELTA; // deg

// Heatbeat LED settings
const int HBEAT_LED_MAX = 245;
const int HBEAT_LED_MIN = 10;

// Moving average filter
const float AVG_NUM_ELTS = 10;
const int MOVING_AVG_MAX = 100; // cm, max distance to still be valid
float filtDist;  // Moving average

const int MOVING_AVG_MAX_CNT = 5;
int movingAvgMaxCnt = 0;

// FSM Definitions
enum FsmState {DOOR_OPEN, DOOR_CLOSED};
enum DoorState {CLOSING_1, CLOSING_2, CLOSED};
FsmState state = DOOR_CLOSED;
DoorState doorState = CLOSED;

const int OPEN_COUNT_INIT = 15;   // Ultrasonic must read distance greater than threshold for this # consecutive times to open door
int openCount = OPEN_COUNT_INIT;


// Servo
Servo lidServo;
float closingAngle = DEG_SRV_CLSD2;
unsigned long closingTime;
const float CLOSING_DELTA = 0.3; // deg



void setup() {
  // Init pins
  pinMode(PIN_ULTSNC_TRIG, OUTPUT);
  pinMode(PIN_ULTSNC_ECHO, INPUT);
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_SERVO, OUTPUT);

  // Init servo
  lidServo.attach(PIN_SERVO);

  Serial.begin(115200);

  // Initialize distance
  filtDist = dist();
  delay(5);
  filtDist = (dist() + filtDist) / 2;

  Serial.print("Init distance (cm): "); Serial.println(filtDist);
}

void loop() {
  // Read and filter ultrasonic distance
  unsigned long currDist = dist();
  movingAvg(currDist);

  if (millis() % 10 == 0){Serial.print("Ultrasonic dist: "); Serial.println(filtDist);}

  // Detect and update state
  switch (state){
    case DOOR_OPEN:
      // Open door
      lidServo.write(DEG_SRV_OPEN);

      // Update state
      if (millis() >= closingTime){
        Serial.print("Door closing time passed, dist: "); Serial.println(filtDist);
        if (filtDist < DOOR_OPEN_DIST_THRESH){
          // Time's up and safe to close door
          state = DOOR_CLOSED;
          doorState = CLOSING_1;
          closingAngle = DEG_SRV_OPEN;

          // Init counter for opening door
          openCount = OPEN_COUNT_INIT;
  
          Serial.println("Door closing");
        }
      }

      break;
      
    case DOOR_CLOSED:
      // Close door slowly
      switch (doorState){
        case CLOSING_1:
          if (closingAngle > DEG_SRV_CLSD1 + 0.5){
            closingAngle -= CLOSING_DELTA;
//            Serial.print("Door closing angle: "); Serial.println(closingAngle);
          } else {
            doorState = CLOSING_2;
          }
          break;
        case CLOSING_2:   // Rise a bit
        case CLOSED:
          closingAngle = DEG_SRV_CLSD2;
          doorState = CLOSED;
          break;
      }
      
      lidServo.write(closingAngle);

      // Update state
      if (filtDist >= DOOR_OPEN_DIST_THRESH){
        openCount--;
        Serial.print("Open count: "); Serial.println(openCount);
        
        if (openCount <= 0){
          // Door has opened, schedule closing
          state = DOOR_OPEN;
          closingTime = millis() + DOOR_OPEN_DURATION;
  
          Serial.print("Door opening at "); Serial.print(millis()); Serial.print("ms and closing at "); Serial.println(closingTime);
        }
      } else {
        openCount = OPEN_COUNT_INIT;
      }
  }

  // Update status LED (enable only when door open)
  if (state == DOOR_OPEN || state == DOOR_CLOSED && doorState != CLOSED){
    heartbeat();
  } else {
    analogWrite(PIN_LED, 0);
  }
  
  delay(5);
}

// Returns ultrasonic distance
unsigned long dist(){
  digitalWrite(PIN_ULTSNC_TRIG, HIGH);
  delayMicroseconds(DLY_ULTSNC_TRIG);
  digitalWrite(PIN_ULTSNC_TRIG, LOW);

  // Return distance
  return pulseIn(PIN_ULTSNC_ECHO, HIGH, ULTSNC_TRIG_TIMEOUT) / TO_CM;
}

// Fade LED in and out
void heartbeat(){
  static int delta = 1;
  static int brightness = HBEAT_LED_MIN + delta;
  if (brightness == HBEAT_LED_MAX || brightness == HBEAT_LED_MIN){
    delta *= -1;
  }

  brightness += delta;
  analogWrite(PIN_LED, brightness);
}

// Moving average filter
void movingAvg(unsigned long newVal){
  if (newVal > MOVING_AVG_MAX){
    // Make sure distance is stable for a count before applying
    if (movingAvgMaxCnt < MOVING_AVG_MAX_CNT){
      movingAvgMaxCnt++;
      Serial.print("MOVING_AVG_MAX exceeded. Count: "); Serial.println(movingAvgMaxCnt);
      return;
    }
    newVal = MOVING_AVG_MAX;
  } else {
    // Reset count if less than max
    movingAvgMaxCnt = 0;
  }

  // Average distance
  filtDist = newVal / AVG_NUM_ELTS + (AVG_NUM_ELTS - 1) / AVG_NUM_ELTS * filtDist;
}

